package org.example.util;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

/**
 *@ClassName: mapStringToMapUtil
 *@Author: CJ
 *@Date: 2021-9-25 14:46
 */
public class MapStringToMapUtil {

    @NotNull
    public static Map<String, String> getStringMap(String str) {
        str = str.substring(1, str.length() - 1);
        String[] strs = str.split(",", 3);
        Map<String, String> map = new HashMap<>();
        for (String string : strs) {
            String key = string.split("=")[0].trim();
            String value = string.split("=")[1];
            map.put(key, value);
        }
        return map;
    }
}
