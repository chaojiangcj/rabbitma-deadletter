package org.example.controller;

import org.example.constant.Constant;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *@ClassName: SendMessageController
 *@Author: CJ
 *@Date: 2021-9-23 17:19
 */
@Controller
public class SendMessageController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 死信队列接口1
     *
     * @Author: CJ
     * @Date: 2021-9-25 13:20
     * @return: java.lang.String
     */
    @GetMapping("/sendDeadLetterMessage1")
    @ResponseBody
    public String sendDeadLetterMessage1() {
        String messageId = String.valueOf(UUID.randomUUID());
        String messageData = "message: dead.letter.business.exchange test message ";
        String createTime = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        Map<String, Object> map = new HashMap<>();
        map.put("messageId", messageId);
        map.put("messageData", messageData);
        map.put("createTime", createTime);
        rabbitTemplate.convertAndSend(Constant.BUSINESS_EXCHANGE, Constant.BUSINESS_QUEUE_A_ROUTING_KEY, map);
        return "消息已发送至rabbitmq server";
    }
}
