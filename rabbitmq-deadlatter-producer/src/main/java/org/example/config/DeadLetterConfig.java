package org.example.config;

import org.example.constant.Constant;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitAdmin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

/**
 * 死信队列配置类
 *
 *@ClassName: DeadLetterConfig
 *@Author: CJ
 *@Date: 2021-9-24 22:05
 */
@Configuration
public class DeadLetterConfig {

    // --------------------------正常业务队列--------------------------
    // 业务队列A
    @Bean
    public Queue businessQueueA() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange", Constant.DEAD_LETTER_EXCHANGE);
        args.put("x-dead-letter-routing-key", Constant.DEAD_LETTER_QUEUE_A_ROUTING_KEY);
        // args.put("x-max-length", 2);
        args.put("x-message-ttl", 5000);
        return new Queue(Constant.BUSINESS_QUEUE_A, true, false, false, args);
    }

    // 业务队列B
    @Bean
    public Queue businessQueueB() {
        Map<String, Object> args = new HashMap<>();
        args.put("x-dead-letter-exchange", Constant.DEAD_LETTER_EXCHANGE);
        args.put("x-dead-letter-routing-key", Constant.DEAD_LETTER_QUEUE_B_ROUTING_KEY);
        return new Queue(Constant.BUSINESS_QUEUE_B, true, false, false, args);
    }

    // 业务队列的交换机
    @Bean
    public TopicExchange businessTopicExchange() {
        return new TopicExchange(Constant.BUSINESS_EXCHANGE, true, false);
    }

    // 业务队列A与交换机绑定，并指定Routing_Key
    @Bean
    public Binding businessBindingA() {
        return BindingBuilder.bind(businessQueueA()).to(businessTopicExchange()).with(Constant.BUSINESS_QUEUE_A_ROUTING_KEY);
    }

    // 业务队列B与交换机绑定，并指定Routing_Key
    @Bean
    public Binding businessBindingB() {
        return BindingBuilder.bind(businessQueueB()).to(businessTopicExchange()).with(Constant.BUSINESS_QUEUE_B_ROUTING_KEY);
    }

    // --------------------------死信队列--------------------------
    // 死信队列A
    @Bean
    public Queue deadLetterQueueA() {
        return new Queue(Constant.DEAD_LETTER_QUEUE_A);
    }

    // 死信队列B
    @Bean
    public Queue deadLetterQueueB() {
        return new Queue(Constant.DEAD_LETTER_QUEUE_B);
    }

    // 死信交换机
    @Bean()
    public DirectExchange deadLetterDirectExchange() {
        return new DirectExchange(Constant.DEAD_LETTER_EXCHANGE);
    }

    // 死信队列A与死信交换机绑定，并指定Routing_Key
    @Bean
    public Binding deadLetterBindingA() {
        return BindingBuilder.bind(deadLetterQueueA()).to(deadLetterDirectExchange()).with(Constant.DEAD_LETTER_QUEUE_A_ROUTING_KEY);
    }

    // 死信队列B与死信交换机绑定，并指定Routing_Key
    @Bean
    public Binding deadLetterBindingB() {
        return BindingBuilder.bind(deadLetterQueueB()).to(deadLetterDirectExchange()).with(Constant.DEAD_LETTER_QUEUE_B_ROUTING_KEY);
    }

    // --------------------------使用RabbitAdmin启动服务便创建交换机和队列--------------------------
    @Bean
    public RabbitAdmin rabbitAdmin(ConnectionFactory connectionFactory) {
        RabbitAdmin rabbitAdmin = new RabbitAdmin(connectionFactory);
        // 只有设置为 true，spring 才会加载 RabbitAdmin 这个类
        rabbitAdmin.setAutoStartup(true);
        // 创建死信交换机和对列
        rabbitAdmin.declareExchange(deadLetterDirectExchange());
        rabbitAdmin.declareQueue(deadLetterQueueA());
        rabbitAdmin.declareQueue(deadLetterQueueB());
        // 创建业务交换机和对列
        rabbitAdmin.declareExchange(businessTopicExchange());
        rabbitAdmin.declareQueue(businessQueueA());
        rabbitAdmin.declareQueue(businessQueueB());
        return rabbitAdmin;
    }
}
