package org.example.deadletter;

import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.example.util.MapStringToMapUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

/**
 * 消息手动接收确认机制，消费者选择使用哪种确认方式
 *
 *@ClassName: MyAckReceiver
 *@Author: CJ
 *@Date: 2021-9-24 14:33
 */
@Slf4j
@Component
public class DeadLetterAckReceiver {

    // 业务队列手动确认消息
    @RabbitListener(queues = "dead.letter.business.queuea")
    @RabbitHandler
    public void deadLetterReceiver1(@NotNull Message message, Channel channel) {
        try {
            // 直接拒绝消费该消息，后面的参数一定要是false，否则会重新进入业务队列，不会进入死信队列
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
            log.info("拒绝签收...消息的路由键为：" + message.getMessageProperties().getReceivedRoutingKey());
        } catch (Exception e) {
            log.info("消息拒绝签收失败", e);
        }
    }

    // 业务队列手动确认消息
    @RabbitListener(queues = "dead.letter.business.queueb")
    @RabbitHandler
    public void deadLetterReceiver2(@NotNull Message message, Channel channel) {
        try {
            // 直接拒绝消费该消息，后面的参数一定要是false，否则会重新进入业务队列，不会进入死信队列
            channel.basicReject(message.getMessageProperties().getDeliveryTag(), false);
            log.info("拒绝签收...消息的路由键为：" + message.getMessageProperties().getReceivedRoutingKey());
        } catch (Exception e) {
            log.info("消息拒绝签收失败", e);
        }
    }
}
